环境条件:jdk8,eclipse,maven，mysql

1、创建数据库：执行jfinal-layui.sql文件

2、修改config-dev.txt数据库配置参数

3、运行com.qinhailin.common.config.MainConfig.java的main方法即可，
     若端口占用问题，可修改undertow.txt相关参数
     
4、默认访问地址：http://localhost

5、管理员登录账号：admin  密码：123456



打包部署到服务器：
1：eclipse 打包：run as -->maven package

2：打包完后，进入 JFinal-layu/target/JFinal-layu-release/JFinal-layui/config 目录，

3:把devMode的值改成false，否则404错误，这个是因为用@ControllerBind注解路由的问题，开发环境可以忽略   
     
4：windows 下双击 start.bat 启动项目，
	linux 下运行 start.sh 脚本启动项目， 运行 stop.sh 关闭项目 
      
注意 JFinal-layui/target 目录下面还会有一个 JFinal-layui-release.zip 文件
	只需要把JFinal-layui-release.zip拷贝到服务器，然后解压,安装打包部署步骤配置运行即可。


	
如果出现404问题处理步骤：
1、把devMode的值改成false，
2、把项目源码的jfinal-layui-1.0.jar复制一份到webapp\WEB-INF\lib目录下，
3、确保MainConfig配置的这行代码，打包部署要是为true
	autoBindRoutes.includeAllJarsInLib(!p.getBoolean("devMode"));
